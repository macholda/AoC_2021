#include <stdio.h>
#include <stdlib.h>

int main(void) {
    FILE *file = fopen("list", "r");
    
    int count_larger = 0;

    int meas;
    int prev_meas = 10000;
    while (fgets (str , 6, file) != NULL) {
        meas = atoi(str);
        if (meas > prev_meas) {
            ++count_larger;
        }
        prev_meas = meas;
    }
    printf("Precisely %u measurements are larger than the previous one.\n", count_larger);
    
    return 0;
}
