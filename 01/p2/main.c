#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define len 2000

int main(void)
{
    FILE *file = fopen("list", "r");

    int *arr[len];

    if (arr == NULL)
    {
        fprintf(stderr, "ERROR: Could not allocate memory: %s\n", strerror(errno));
    }

    char *str = malloc(6 * sizeof(char));
    for (int i = 0; i < len; ++i)
    {
        arr[i] = malloc(sizeof(int));
        fgets(str, 6, file);
        *arr[i] = atoi(str);        
    }

    int count;
    int buf1;
    int buf2 = *arr[0] + *arr[1] + *arr[2];
    for (int i = 0; i < len-3; ++i)
    {
        buf1 = buf2;
        buf2 = *arr[i + 1] + *arr[i + 2] + *arr[i + 3];

        printf("%u + %u + %u = %u\n", *arr[i], *arr[i + 1], *arr[i + 2], buf1);
        printf("%u + %u + %u = %u\n", *arr[i + 1], *arr[i + 2], *arr[i + 3], buf2);

        if (buf2 > buf1)
        {
            ++count;
            printf("Increasing: %u\n", count);
        }
        printf("-------------------------\n");
    }

    printf("The number has increased a total of %u times.\n", count);

    return 0;
}
