#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#define len 12

char *returnBin(FILE *file)
{
    char *str = malloc(len * sizeof(char));
    if(fscanf(file, "%s", str) == EOF) return NULL;
    return str;
}

int main(void)
{
    FILE *file = fopen("input", "r");
    char *gamma = malloc(len * sizeof(char));
    for(uint8_t i = 0; i < len; ++i)
    {
        char *str = returnBin(file);
        int count = 0;
        while(str != NULL)
        {
            char bit = *(str + i);

            printf("Line: %s\n", str);
            printf("Inspected bit: %c\n", bit);
            if(bit == 49) ++count;
            printf("Current count: %i\n", count);
            printf("i: %d\n", i);
            str = returnBin(file);
        }
        (count > 1000/2) ? strcpy((gamma + i), "1") : strcpy((gamma + i), "0");
        rewind(file);
    }
    int gamma_int = 0;
    int epsilon_int = 0;

    for(uint8_t i = 0; i < len; ++i)
    {
        char bit = *(gamma + len - i - 1);
        if(bit == '1') gamma_int += pow(2, i);
        else epsilon_int += pow(2, i);
    }        

    printf("The multiple of gamma and epsilon is: %d\n", gamma_int*epsilon_int);
    
    return 0;

}
