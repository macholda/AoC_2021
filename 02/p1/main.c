#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

char *ReturnDirection(FILE *file)
{
    char *str = malloc(8 * sizeof(char));
    if(fscanf(file, "%s", str) == EOF) return NULL;
    return str;
}

int ReturnVal(FILE *file)
{
    int val;
    fscanf(file, "%d", &val);
    return val;
}

int main(void)
{
    FILE *file = fopen("list", "r");
    int horizontal = 0;
    int depth = 0;

    char *direction = ReturnDirection(file);
    int val = ReturnVal(file);
    while(direction != NULL)
    {
        if(!strcmp(direction, "forward")) horizontal += val;
        if(!strcmp(direction, "down")) depth += val;
        if(!strcmp(direction, "up")) depth -= val;
        printf("Direction: %s\n", direction);
        printf("Val: %d\n\n", val);
        printf("Horizontal: %d\n", horizontal);
        printf("Depth: %d\n", depth);
        printf("-------------------\n");
    
        direction = ReturnDirection(file);
        val = ReturnVal(file);
    }
    
    printf("The multiple of final horizontal position and depth is: %d\n",
           horizontal*depth);
    return 0;
}
